import React, {
  createContext, useCallback, useContext, useState,
} from 'react';
import api from '../services/api';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [data, setData] = useState(() => {
    const token = localStorage.getItem('@securitycom:token');
    const client = localStorage.getItem('@securitycom:client');
    const uid = localStorage.getItem('@securitycom:uid');

    if (token && client && uid) {
      return { token, client, uid };
    }

    return {};
  });

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('/users/auth/sign_in', {
      email,
      password,
    });
    const { headers } = response;

    localStorage.setItem('@securitycom:token', headers['access-token']);
    localStorage.setItem('@securitycom:client', headers.client);
    localStorage.setItem('@securitycom:uid', headers.uid);

    setData({
      token: headers['access-token'],
      client: headers.client,
      uid: headers.uid,
    });
  }, []);

  return (
    <AuthContext.Provider value={{ signIn, data }}>
      { children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth deve ser utilizado dentro de um AuthProvider');
  }

  return context;
};
