import React, {
  createContext, useCallback, useContext, useState,
} from 'react';
import api from '../services/api';
import { useAuth } from './Auth';

const CompanyContext = createContext();

export const CompanyProvider = ({ children }) => {
  const [enterprises, setEnterprises] = useState([]);
  const [enterpriseDetail, setEnterpriseDetail] = useState({});
  const [titleSearch, setTitleSearch] = useState('');
  const { data } = useAuth();
  const searchCompany = useCallback(async ({ company }) => {
    const response = await api.get(`enterprises?name=${company}`, {
      headers: {
        'access-token': data.token,
        client: data.client,
        uid: data.uid,
      },
    });

    setEnterprises(response.data.enterprises);
    setTitleSearch(company);
  }, [data]);

  const clearSearch = useCallback(() => {
    setEnterprises([]);
    setTitleSearch('');
  }, []);

  const getDetailEnterprise = useCallback(async ({ idCompany }) => {
    const response = await api.get(`enterprises/${idCompany}`, {
      headers: {
        'access-token': data.token,
        client: data.client,
        uid: data.uid,
      },
    });

    setEnterpriseDetail(response.data.enterprise);
  }, [data]);

  return (
    <CompanyContext.Provider value={{
      searchCompany, enterprises, titleSearch, clearSearch, getDetailEnterprise, enterpriseDetail,
    }}
    >
      { children}
    </CompanyContext.Provider>
  );
};

export const useCompany = () => {
  const context = useContext(CompanyContext);

  if (!context) {
    throw new Error('useAuth deve ser utilizado dentro de um CompanyProvider');
  }

  return context;
};
