import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --white: #d8d8d8;
    --white-two: #ffffff;
    --dark-indigo: #1a0e49;
    --warm-grey: #8d8c8c;
    --charcoal-grey-two: #403e4d;
    --greyish: #b5b4b4;
    --medium-pink: #ee4c77;
    --night-blue: #0d0430;
    --charcoal-grey: #383743;

    --spacing-xxs: 0.25rem;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 500;
  }

  button {
    cursor: pointer;
  }

  ul {
    list-style-type: none;
  }

  a {
    text-decoration: none;
  }

  img {
    max-width: 100%;
    display: block;
  }
`;
