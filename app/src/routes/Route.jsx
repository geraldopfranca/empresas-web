import React from 'react';
import { Redirect, Route as ReactDOMRoute } from 'react-router-dom';

import { useAuth } from '../hooks/Auth';

const PrivateRoute = ({ children, ...rest }) => {
  const { data } = useAuth();
  const isAuthenticated = !!data.token;

  return (
    <ReactDOMRoute
      {...rest}
      render={() => (isAuthenticated ? children : <Redirect to="/" />)}
    />
  );
};

export default PrivateRoute;
