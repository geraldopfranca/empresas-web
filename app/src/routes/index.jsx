import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';

import PrivateRoute from './Route';

import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import EnterpriseDetail from '../pages/EnterpriseDetail';

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/"><Login /></Route>

      <PrivateRoute path="/dashboard"><Dashboard /></PrivateRoute>
      <PrivateRoute path="/detail/:company"><EnterpriseDetail /></PrivateRoute>
    </Switch>
  </Router>
);

export default Routes;
