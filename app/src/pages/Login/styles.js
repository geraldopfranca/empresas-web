import styled from 'styled-components'

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #eeecdb;

  img {
    height: 72px;
  }

  form {
    display: flex;
    flex-direction: column;
    width: 348px;

    h1 {
      display: flex;
      width: 172px;
      height: 60px;
      margin: 65px 92px 25px 90px;
      font-family: Roboto;
      font-size: 25.6px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: -1.2px;
      text-align: center;
      color: var(--charcoal-grey);
    }

    .textLogin {
      width: 348px;
      height: 50px;
      margin: 25px 4px 46px 10px;
      font-family: Roboto;
      font-size: 17px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.48;
      letter-spacing: 0.2px;
      text-align: center;
      color: var(--charcoal-grey);
    }

    .inputLogin {
      height: 25px;
      border: none;
      font-family: Roboto;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: -0.31px;
      color: var(--charcoal-grey-two);
    }

    .btnSearch {
      height: 57px;
      border: none;
      margin: 41px 0 0;
      padding: 18px 134px 16px 135px;
      border-radius: 3.9px;
      background-color: #57bbbc;

      font-family: Roboto;
      font-size: 19.3px;
      font-weight: 500;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: var(--white-two);
    }

  }
`

export const Error = styled.span`
  width: 332px;
  height: 19px;
  margin: 12px 7px -26px 15px;
  font-family: Roboto;
  font-size: 12.2px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
  color: #ff0f44;
`
