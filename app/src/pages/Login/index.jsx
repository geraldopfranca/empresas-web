import React, { useCallback, useState } from 'react';

import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/Auth';
import Input from '../../components/Input';

import Logo from '../../assets/logo-home@3x.png';
import IcoEmail from '../../assets/ic-email.svg';
import IcoCadeado from '../../assets/ic-cadeado.svg';
import { Container, Error } from './styles';

const Login = () => {
  const [email, setEmail] = useState('testeapple@ioasys.com.br');
  const [password, setPassword] = useState('12341234');
  const [error, setError] = useState('');

  const { signIn } = useAuth();
  const history = useHistory();

  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();

    try {
      setError('');
      await signIn({ email, password });
      history.push('/dashboard');
    } catch (err) {
      if (err.response) {
        if (err.response.status === 401) {
          setError('Credenciais informadas são inválidas, tente novamente.');
        } else {
          setError(`Não foi possível realizar o login. Código do erro: ${err.response.status}`);
        }
      } else {
        setError(`Não foi possível identificar o erro. Mensagem: ${err.message}`);
      }
    }
  }, [email, history, password, signIn]);

  return (
    <Container>
      <img src={Logo} alt="Empresa Web" />
      <form onSubmit={(e) => handleSubmit(e)}>

        <h1>BEM-VINDO AO EMPRESAS</h1>
        <span className="textLogin">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</span>

        <Input
          name="email"
          type="text"
          className="inputLogin"
          placeholder="E-mail"
          icon={IcoEmail}
          onChange={(e) => setEmail(e.target.value)}
          isErrored={error.length > 1}
        />
        <Input
          name="password"
          type="password"
          className="inputLogin"
          placeholder="Senha"
          icon={IcoCadeado}
          onChange={(e) => setPassword(e.target.value)}
          isErrored={error.length > 1}
        />
        { error.length > 1 && <Error>{ error }</Error>}
        <button className="btnSearch" type="submit">
          ENTRAR
        </button>
      </form>
    </Container>
  );
};

export default Login;
