import styled from 'styled-components';

export const Container = styled.div`
  heigth: 100vh;
  display: flex;
  justify-content: center;
  padding: 44px 20px 44px 20px;
  background-color: #ebe9d7;

  .details {
    background-color: var(--white-two);
    padding: 26px 29px 26px 29px;
    margin-bottom: 29px;
    margin-left: 10px;
    width: 923px;
    border-radius: 4.8px;
  }

  .img {
    display: flex;
    justify-content: center;
    height: 295px;
  }

  .description {
    margin: 48px 12px 5px 14px;
    font-family: SourceSansPro;
    font-size: 28px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: var(--warm-grey);
  }
`
