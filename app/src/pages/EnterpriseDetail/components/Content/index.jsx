import React from 'react';

import { Container } from './styles';

const Content = ({ data }) => (

  <Container>
    <div className="details">
      <div className="img">
        <img src={`https://empresas.ioasys.com.br${data.photo}`} alt="" />
      </div>
      <div className="description">
        <span>{data.description}</span>
      </div>
    </div>
  </Container>
);

export default Content;
