import React from 'react';
import { FiArrowLeft } from 'react-icons/fi';
import { useHistory } from 'react-router-dom';

import { Container } from './styles';

const Header = ({ enterprise }) => {
  const history = useHistory();
  return (
    <Container>
      <button type="button" onClick={() => { history.goBack(); }} className="goback">
        <FiArrowLeft color="#fff" size={50} className="arrowLeft" />
      </button>
      <span className="enterprisename">{enterprise}</span>
    </Container>
  );
};

export default Header;
