import styled from 'styled-components';

export const Container = styled.div`
  height: 150px;
  background-image: linear-gradient(173deg, #ed4b76, #bc3c67);
  display: flex;

  .goback {
    border: none;
    background: transparent;
  }

  .arrowLeft {
    margin: 35px 0 0 50px;
  }

  .enterprisename {
    margin: 71px 0 0 68px;
    height: 40px;
    font-family: Roboto;
    font-size: 34px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: var(--white-two);
  }
`

