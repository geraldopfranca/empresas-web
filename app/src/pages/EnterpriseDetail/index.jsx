import React, { useEffect } from 'react';

import { useRouteMatch } from 'react-router-dom';
import Header from './components/Header';
import Content from './components/Content';
import { useCompany } from '../../hooks/Company';

const EnterpriseDetail = (props) => {
  const { getDetailEnterprise, enterpriseDetail } = useCompany();
  const { params } = useRouteMatch();

  useEffect(() => {
    getDetailEnterprise({ idCompany: params.company });
  }, [getDetailEnterprise, params.company]);

  return (
    <>
      <Header enterprise={enterpriseDetail.enterprise_name} />
      <Content data={enterpriseDetail} />
    </>
  );
};

export default EnterpriseDetail;
