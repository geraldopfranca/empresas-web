import React, { useEffect, useState } from 'react';

import Header from '../../components/Layout/Header';
import { useCompany } from '../../hooks/Company';
import Content from './components/Content';

const Dashboard = () => {
  const { searchCompany } = useCompany();
  return (
    <>
      <Header search={false} />
      <Content search={false} />
    </>
  );
};

export default Dashboard;
