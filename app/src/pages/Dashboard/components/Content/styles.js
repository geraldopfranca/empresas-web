import styled, { css } from 'styled-components'

export const Container = styled.div`
  height: 85vh;
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  padding: 44px 20px 44px 20px;
  background-color: #ebe9d7;

  ${props =>
    props.isSearch &&
    css`
      height: 100%;
    `
  }

  img {
    width: 293px;
    height: 160px;
  }

  span {
    font-family: Roboto;
    font-size: 32px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.22;
    letter-spacing: -0.45px;
    text-align: center;
    color: var(--charcoal-grey);
  }

  li {
    background-color: var(--white-two);
    padding: 26px 29px 26px 29px;
    margin-bottom: 29px;
    margin-left: 10px;
    width: 923px;

    header {
      display: flex;
      flex-direction: row;
    }

    .company {
      display: flex;
      flex: 1;
      flex-direction: column;

      .title {
        margin: 23px 0 0 39.4px;
        font-family: Roboto;
        font-size: 30px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: var(--dark-indigo);
      }

      .segment {
        width: 172.6px;
        height: 32px;
        margin: 10px 0 0 39.4px;
        font-family: Roboto;
        font-size: 24px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: var(--warm-grey);
      }

      .country {
        width: 119.7px;
        height: 16px;
        margin: 4px 187.6px 38px 39.4px;
        font-family: Roboto;
        font-size: 18px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: var(--warm-grey);
      }
    }
  }

`;

