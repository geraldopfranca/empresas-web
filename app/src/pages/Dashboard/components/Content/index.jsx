import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useCompany } from '../../../../hooks/Company';
import { Container } from './styles';

const Content = ({ search }) => {
  const { enterprises, titleSearch } = useCompany();
  const [message, setMessage] = useState('Clique na busca para iniciar.');

  useEffect(() => {
    setMessage('Clique na busca para iniciar.');
    if (enterprises.length === 0 && titleSearch.length > 0) {
      setMessage('Nenhuma empresa foi encontrada para a busca realizada.');
    }
  }, [enterprises, titleSearch]);

  return (
    <Container isSearch={enterprises.length > 0}>
      {enterprises.length > 0
        ? <Enterprises data={enterprises} />
        : <span>{ message }</span>}
    </Container>
  );
};

const Enterprises = ({ data }) => (
  <ul>
    {data.map((item) => (
      <li>
        <header>
          <Link to={`/detail/${item.id}`}>
            <img src={`https://empresas.ioasys.com.br${item.photo}`} alt="" />
          </Link>
          <div className="company">
            <h1 className="title">{item.enterprise_name}</h1>
            <strong className="segment">Teste</strong>
            <span className="country">{`${item.city} - ${item.country}`}</span>
          </div>
        </header>
      </li>
    ))}
  </ul>
);

export default Content;
