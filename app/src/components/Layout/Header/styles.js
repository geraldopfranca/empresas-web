import styled from 'styled-components'
import Logo from '../../../assets/logo-nav.png'
import IcoSearch from '../../../assets/ic-search.svg'
import IcoClose from '../../../assets/ic-close.svg';

export const Container = styled.div`
  height: 150px;
  background-image: linear-gradient(173deg, #ed4b76, #bc3c67);
  display: flex;
  justify-content: center;
  align-items: center;

  .logo {
    height: 57px;
    width: 75%;
    background-image: url(${Logo});
    background-position: center;
    background-repeat:no-repeat;
  }

  button{
    width: 50px;
    height: 50px;
    border: none;
    background: transparent;
    background-image: url(${IcoSearch});
    background-position: center;
    background-repeat: no-repeat;
    :hover{
      opacity: 0.7;
    }
  }
`

export const Content = styled.div`
  display: flex;
  flex: 1;
  background: #ebe9d7;
  justify-content: center;

  span {
    width: 546px;
    height: 39px;
    margin: 359px 239px 0 0;
    font-family: Roboto;
    font-size: 32px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.22;
    letter-spacing: -0.45px;
    text-align: center;
    color: var(--charcoal-grey);
  }
`
export const Search = styled.div`
  height: 150px;
  background-image: linear-gradient(173deg, #ed4b76, #bc3c67);
`
export const InputSearch = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 60px 10px 0 10px;
  margin: 0 30px 0 30px;

  border-bottom: solid 1px var(--white-two);

  input {
    border: none;
    background: transparent;
    margin: 10px;

    font-family: Roboto;
    font-size: 34px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: left;
    width: 100%;
    color: var(--white-two);

    ::placeholder {
      color: #991237;
    }
  }

  button{
    width: 50px;
    height: 50px;
    border: none;
    background: transparent;
    background-image: url(${IcoClose});
    background-position: center;
    background-repeat: no-repeat;
    :hover{
      opacity: 0.7;
    }
  }
`
