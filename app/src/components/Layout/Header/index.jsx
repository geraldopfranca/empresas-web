import React, {
  useCallback, useRef, useState,
} from 'react';

import { useForm } from 'react-hook-form';
import IcoSearch from '../../../assets/ic-search.svg';

import {
  Container, Search, InputSearch,
} from './styles';
import { useCompany } from '../../../hooks/Company';

const Header = ({ search }) => {
  const formRef = useRef(null);
  const [visibleSearch, setVisibleSearch] = useState(search);
  const { register, handleSubmit } = useForm();
  const { searchCompany, clearSearch } = useCompany();

  const onClickClose = () => {
    setVisibleSearch(!visibleSearch);
    clearSearch();
  };

  const onChangeGetCompany = useCallback(async (value) => {
    if (value.length > 1) {
      searchCompany({ company: value });
    }
  }, [searchCompany]);

  return (
    <>
      {visibleSearch
      && (
      <Search>
        <form ref={formRef}>
          <InputSearch>
            <img src={IcoSearch} alt="" />
            <input
              ref={register({ required: true })}
              type="text"
              name="search"
              placeholder="Pesquisar"
              onChange={(e) => onChangeGetCompany(e.target.value)}
            />
            <button onClick={onClickClose} type="button" />
          </InputSearch>
        </form>
      </Search>
      )}
      {!visibleSearch
      && (
        <Container>
          <div className="logo" />
          <button onClick={() => setVisibleSearch(!visibleSearch)} type="button" />
        </Container>
      )}
    </>
  );
};

export default Header;
