import styled, { css } from 'styled-components';

export const InputContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: 30px;

  border-bottom: solid 0.6px var(--charcoal-grey);

  ${props =>
    props.isErrored &&
    css`
      border-bottom: solid 0.6px #c53030;
    `
  }

  .img {
    width: 26px;
    height: 26px;
    margin-top: -3px;
  }

  input {
    width: 100%;
    border: none;
    background: transparent;
    margin: 3px 91px 5px 5px;
    opacity: 0.5;
    font-family: Roboto;
    font-size: 18px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.25px;
    text-align: left;
    color: var(--charcoal-grey);

    ::placeholder {
      color: var(--charcoal-grey-two);
    }
  }

`;

export const Alert = styled.div`
  height: 20px;
  span {
    background: #c53030;
    color: #fff;
    &::before {
      border-color: #c53030 transparent;
    }
  }

  .btnAlert {
    border: none;
    background: #black-54;
  }
`;
