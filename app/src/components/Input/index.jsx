import React, { useState } from 'react';
import { FiAlertCircle, FiEye } from 'react-icons/fi';

import { InputContainer, Alert } from './styles';

const InputSearch = ({
  icon, hint, isErrored = false, ...rest
}) => {
  const [isVisiblePassword, setIsVisiblePassword] = useState(false);

  return (
    <InputContainer isErrored={isErrored}>
      { icon && <img src={icon} alt={hint} className="img" />}
      <input {...rest} type={isVisiblePassword ? 'text' : rest.type} />
      {rest.type === 'password' && !isErrored
      && (
      <Alert>
        <button type="button" onClick={(e) => setIsVisiblePassword(!isVisiblePassword)} className="btnAlert">
          <FiEye color="#black-54" size={20} />
        </button>
      </Alert>
      )}

      {isErrored
      && (
      <Alert>
        <FiAlertCircle color="#c53030" size={20} />
      </Alert>
      )}
    </InputContainer>
  );
};

export default InputSearch;
