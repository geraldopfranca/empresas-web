import React from 'react'
import GlobalStyle from './styles/global'
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes';

import { AuthProvider } from './hooks/Auth';
import { CompanyProvider } from './hooks/Company';

const App = () => (
  <Router>
    <AuthProvider>
      <CompanyProvider>
        <Routes />
      </CompanyProvider>
    </AuthProvider>

    <GlobalStyle />
  </Router>
)

export default App;
